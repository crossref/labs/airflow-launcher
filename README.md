# Airflow Launcher
This library allows for the easy construction and management of Dask clusters from Amazon Managed Workflows for Apache Airflow. It automatically bootstraps the MWAA environment with the necessary binaries and pulls a git repository for execution.

![license](https://img.shields.io/gitlab/license/crossref/labs/airflow-launcher) ![activity](https://img.shields.io/gitlab/last-commit/crossref/labs/airflow-launcher) <a href="https://github.com/psf/black"><img alt="Code style: black" src="https://img.shields.io/badge/code%20style-black-000000.svg"></a>

![Airflow](https://img.shields.io/badge/Airflow-017CEE?style=for-the-badge&logo=Apache%20Airflow&logoColor=white) ![Dask](https://img.shields.io/badge/dask-%23092E20.svg?style=for-the-badge&logo=dask&logoColor=white) ![Git](https://img.shields.io/badge/git-%23F05033.svg?style=for-the-badge&logo=git&logoColor=white) ![GitLab](https://img.shields.io/badge/gitlab-%23121011.svg?style=for-the-badge&logo=gitlab&logoColor=white) ![Linux](https://img.shields.io/badge/Linux-FCC624?style=for-the-badge&logo=linux&logoColor=black) ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54)

## Prerequisites
You will need to use the plugin supplied in the "plugin" folder on your Airflow install. This allows us [to run virtual environments with specific requirements](https://docs.aws.amazon.com/mwaa/latest/userguide/samples-virtualenv.html). 

## Configuration
First, clone the repository and edit launcher.py.

Modify the schedule for execution and [the DistRunner parameters](https://gitlab.com/crossref/labs/distrunner) for your instances.

Note: the requirements on "run_distributed_task" do not need to be updated. The bootstrapping system will automatically install project requirements from the specified git repository's requirements.txt.

## Installation / Usage
Push the launcher.py script to the S3 bucket for your MWAA instance. The "build_helpers" folder contains scripts to push files, but they will require editing as they are specific to Crossref (and 1Password).

If remote distributed workers are failing to bootstrap, please ensure that you have the latest version of Dask installed.

## Credits
* [Apache Airflow](https://airflow.apache.org/)
* [AWS/Boto](https://github.com/boto/botocore)
* [Dask](https://www.dask.org/)
* [Git](https://git-scm.com/)
* [GitPython](https://github.com/gitpython-developers/GitPython)

Copyright &copy; Crossref 2023 