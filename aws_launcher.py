import subprocess
from datetime import datetime
from datetime import timedelta

from airflow.decorators import dag, task

DEFAULT_ARGS = {
    "owner": "meve",
    "depends_on_past": False,
    "email": "labs@crossref.org",
    "email_on_failure": False,
    "email_on_retry": False,
}


@dag(
    default_args=DEFAULT_ARGS,
    schedule_interval="@monthly",
    catchup=False,
    dagrun_timeout=timedelta(hours=16),
    start_date=datetime(2023, 1, 29),
    tags=[
        "example",
    ],
)
def task_example():
    """
    This is an Airflow DAG that will launch a distributed Fargate cluster,
    bootstrap git onto AWS MWAA, pull a git repository to MWAA and all workers,
    and then delegate control to your specified entry point in that repository.

    See README.md for more details, including configuration and usage.
    :return: None
    """

    @task.virtualenv(
        task_id="task_example",
        requirements=["distrunner", "requests"],
        system_site_packages=True,
    )
    def run_distributed_task():
        # set the environment variable so that GitPython doesn't complain
        # before bootstrapping is complete
        from distrunner.distrunner import DistRunner
        import logging

        # launch the distributed task
        # if on Airflow, set on_airflow to True
        # if on AWS Managed Airflow, set on_aws to True
        with DistRunner(
            workers=3,
            python_version="3.10",
            repo="https://gitlab.com/crossref/labs/airflow-daily-new-doi-count-task.git",
            entry_module="task",
            entry_point="entry_point",
            requirements_file="requirements.txt",
            retries=3,
            worker_memory=16384,
            worker_cpus=4096,
            verbose_workers=True,
            local=True,
            on_airflow=True,
            on_aws=True,
        ) as dr:
            logging.basicConfig(level=logging.INFO)

            dr.run()

    task_to_run = run_distributed_task()


task_example()
