#!/bin/bash
op run --env-file="./build_helpers/build.env" -- aws s3api put-object --bucket airflow-crossref-research-annotation --key dags/launcher.py --body aws_launcher.py
# aws s3api put-object --bucket airflow-crossref-research-annotation --key requirements.txt --body requirements.txt
# aws s3api put-object --bucket airflow-crossref-research-annotation --key plugins.zip --body plugins.zip
