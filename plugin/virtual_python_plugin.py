import os
from airflow.plugins_manager import AirflowPlugin
import airflow.utils.python_virtualenv
from typing import List


def _generate_virtualenv_cmd(
    tmp_dir: str, python_bin: str, system_site_packages: bool
) -> List[str]:
    cmd = [
        "python3",
        "/usr/local/airflow/.local/lib/python3.10/site-packages/virtualenv",
        tmp_dir,
    ]
    if system_site_packages:
        cmd.append("--system-site-packages")
    if python_bin is not None:
        cmd.append(f"--python={python_bin}")
    return cmd


airflow.utils.python_virtualenv._generate_virtualenv_cmd = (
    _generate_virtualenv_cmd
)

os.environ["PATH"] = f"/usr/local/airflow/.local/bin:{os.environ['PATH']}"


class VirtualPythonPlugin(AirflowPlugin):
    name = "virtual_python_plugin"
